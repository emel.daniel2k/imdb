<?php

    include('fuggvenyek.php');
    imdb_connect();

    $v_szineszID = $_POST['szineszID'];
    $v_szineszNev = $_POST['szineszNev'];
    $v_szuletesiEv = $_POST['szuletesiEv'];
    $v_szuletesiOrszag = $_POST['szuletesiOrszag'];

    if ( isset($v_szineszID) && isset($v_szineszNev) &&
        isset($v_szuletesiEv) && isset($v_szuletesiOrszag) ) {

        $v_clear_szineszID = htmlspecialchars($v_szineszID);
        $v_clear_szineszNev = htmlspecialchars($v_szineszNev);
        $v_clear_szuletesiEv = htmlspecialchars($v_szuletesiEv);
        $v_clear_szuletesiOrszag = htmlspecialchars($v_szuletesiOrszag);

        // beszúrjuk az új rekordot az adatbázisba
        $sikeres = add_szinesz($v_clear_szineszID, $v_clear_szineszNev, $v_clear_szuletesiEv, $v_clear_szuletesiOrszag);

        if ($sikeres == false){
            die("Nem sikerült felvinni rekordot.");
        } else {
            header("Location: list_szinesz.php");
        }
    } else {
        error_log("Nincs beállítva valamely érték");
    }
//END
