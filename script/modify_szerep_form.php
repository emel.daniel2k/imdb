<?php
include("connect.php");
include("fuggvenyek.php");
?>
<?php
$v_filmID = $_POST["filmID"];
echo $v_filmID;
$v_szineszID = $_POST["szineszID"];
$v_clear_filmID = htmlspecialchars($v_filmID);
$v_clear_szineszID = htmlspecialchars($v_szineszID);
$szerepadat = get_szerep($v_clear_filmID, $v_clear_szineszID); ?>

<h3>Szerep szerkesztése:</h3>
<form method="post" id="szerepForm" class="form-inline" action="modify_szerep_script.php" accept-charset="UTF-8">
    <label for="filmID3">Film ID:</label>
    <?php echo '<input type="number" id="filmID3" name="filmID" min="1" max="9999" value="'.$szerepadat["filmID"].'" readonly="readonly"/>';?>
    <label for="szineszID1">Színész ID:</label>
    <?php echo'<input type="number" id="szineszID1" name="szineszID" min="1" max="9999" value="'.$szerepadat["szineszID"].'" readonly="readonly"/>';?>
    <label for="szerep">Szerep:</label>
    <?php echo '<input type="text" id="szerep" name="szerep" value="'.$szerepadat["szerep"].'"/>';?>
    <button type="submit" form="szerepForm" class="btn btn-primary">Szerkesztés</button>
</form>
