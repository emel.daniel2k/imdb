<?php
include('fuggvenyek.php');
imdb_connect();
$v_filmID = $_POST['filmID'];
$v_szineszID = $_POST['szineszID'];

if ( isset($v_szineszID) && isset($v_filmID)) {
    // beszúrjuk az új rekordot az adatbázisba
    $v_clear_filmID = htmlspecialchars($v_filmID);
    $v_clear_szineszID = htmlspecialchars($v_szineszID);

    $success = remove_szerep($v_clear_filmID, $v_clear_szineszID);
    if($success == false){
        die("Nem sikerült törölni a rekordot.");
    } else {
        header("Location: list_szerep.php");
    }
} else {
    error_log("Nincs beállítva valamely érték");
}
