<?php

function imdb_connect()
{
    $conn = mysqli_connect("localhost", "root", "") or die("Hibás csatlakozás");
    if (false == mysqli_select_db($conn, "imdb")) {
        return null;
    }

    mysqli_query($conn, 'SET NAMES UTF-8');
    mysqli_query($conn, 'SET character_set_results=utf8');
    mysqli_set_charset($conn, 'utf-8');

    return $conn;
}

function add_rendezo($rendezoID, $rendezoNev, $szuletesiEv, $szuletesiOrszag){
    if ( !($conn = imdb_connect()) ) { // ha nem sikerult csatlakozni, akkor kilepunk
        return false;
    }

    // elokeszitjuk az utasitast
    $stmt = mysqli_prepare( $conn,"INSERT INTO rendezo(rendezoID,rendezoNev,szuletesiEv,szuletesiOrszag) VALUES (?, ?, ?, ?)");

    // bekotjuk a parametereket (igy biztonsagosabb az adatkezeles)
    mysqli_stmt_bind_param($stmt, "dsds", $rendezoID, $rendezoNev, $szuletesiEv, $szuletesiOrszag);

    // lefuttatjuk az SQL utasitast
    $success = mysqli_stmt_execute($stmt);
    // ez logikai erteket ad vissza, ami megmondja, hogy sikerult-e
    // vegrehajtani az utasitast

    if($success == false){
        die(mysqli_error($conn));
    }

    mysqli_close($conn);
    return $success;
}

function add_studio($studioID, $studioNev, $alapitasiEv){
    if ( !($conn = imdb_connect()) ) { // ha nem sikerult csatlakozni, akkor kilepunk
        return false;
    }

    // elokeszitjuk az utasitast
    $stmt = mysqli_prepare( $conn,"INSERT INTO studio(studioID,studioNev,alapitasiEv) VALUES (?, ?, ?)");

    // bekotjuk a parametereket (igy biztonsagosabb az adatkezeles)
    mysqli_stmt_bind_param($stmt, "dsd", $studioID, $studioNev, $alapitasiEv);

    // lefuttatjuk az SQL utasitast
    $success = mysqli_stmt_execute($stmt);
    // ez logikai erteket ad vissza, ami megmondja, hogy sikerult-e
    // vegrehajtani az utasitast

    if($success == false){
        die(mysqli_error($conn));
    }

    mysqli_close($conn);
    return $success;
}


function add_mufaj($filmID, $mufaj){
    if ( !($conn = imdb_connect()) ) { // ha nem sikerult csatlakozni, akkor kilepunk
        return false;
    }

    // elokeszitjuk az utasitast
    $stmt = mysqli_prepare( $conn,"INSERT INTO mufaj(filmID,mufaj) VALUES (?, ?)");

    // bekotjuk a parametereket (igy biztonsagosabb az adatkezeles)
    mysqli_stmt_bind_param($stmt, "ds", $filmID, $mufaj);

    // lefuttatjuk az SQL utasitast
    $success = mysqli_stmt_execute($stmt);
    // ez logikai erteket ad vissza, ami megmondja, hogy sikerult-e
    // vegrehajtani az utasitast

    if($success == false){
        die(mysqli_error($conn));
    }

    mysqli_close($conn);
    return $success;
}

function add_szinesz($szineszID,$szineszNev,$szuletesiEv, $szuletesiOrszag){
    if ( !($conn = imdb_connect()) ) { // ha nem sikerult csatlakozni, akkor kilepunk
        return false;
    }

    // elokeszitjuk az utasitast
    $stmt = mysqli_prepare( $conn,"INSERT INTO szinesz(szineszID,szineszNev, szuletesiEv, szuletesiOrszag) VALUES (?, ?, ?, ?)");

    // bekotjuk a parametereket (igy biztonsagosabb az adatkezeles)
    mysqli_stmt_bind_param($stmt, "dsds", $szineszID, $szineszNev, $szuletesiEv, $szuletesiOrszag);

    // lefuttatjuk az SQL utasitast
    $success = mysqli_stmt_execute($stmt);
    // ez logikai erteket ad vissza, ami megmondja, hogy sikerult-e
    // vegrehajtani az utasitast

    if($success == false){
        die(mysqli_error($conn));
    }

    mysqli_close($conn);
    return $success;
}

function add_szerep($filmID,$szineszID,$szerep){
    if ( !($conn = imdb_connect()) ) { // ha nem sikerult csatlakozni, akkor kilepunk
        return false;
    }

    // elokeszitjuk az utasitast
    $stmt = mysqli_prepare( $conn,"INSERT INTO szerep(filmID,szineszID, szerep) VALUES (?, ?, ?)");

    // bekotjuk a parametereket (igy biztonsagosabb az adatkezeles)
    mysqli_stmt_bind_param($stmt, "dds", $filmID, $szineszID, $szerep);

    // lefuttatjuk az SQL utasitast
    $success = mysqli_stmt_execute($stmt);
    // ez logikai erteket ad vissza, ami megmondja, hogy sikerult-e
    // vegrehajtani az utasitast

    if($success == false){
        die(mysqli_error($conn));
    }

    mysqli_close($conn);
    return $success;
}

function add_film($filmID, $cim, $megjelenesiEv, $korhatar, $rendezoID, $studioID){
    if ( !($conn = imdb_connect()) ) { // ha nem sikerult csatlakozni, akkor kilepunk
        return false;
    }

    // elokeszitjuk az utasitast
    $stmt = mysqli_prepare( $conn,"INSERT INTO film(filmID,cim,megjelenesiEv,korhatar, rendezoID, studioID) VALUES (?, ?, ?, ?, ?, ?)");

    // bekotjuk a parametereket (igy biztonsagosabb az adatkezeles)
    mysqli_stmt_bind_param($stmt, "dsdddd", $filmID, $cim, $megjelenesiEv, $korhatar, $rendezoID, $studioID);

    // lefuttatjuk az SQL utasitast
    $success = mysqli_stmt_execute($stmt);
    // ez logikai erteket ad vissza, ami megmondja, hogy sikerult-e
    // vegrehajtani az utasitast

    if($success == false){
        die(mysqli_error($conn));
    }

    mysqli_close($conn);
    return $success;
}

function modify_film ($filmID, $cim, $megjelenesiEv, $korhatar, $rendezoID, $studioID){
    if ( !($conn = imdb_connect()) ) { // ha nem sikerult csatlakozni, akkor kilepunk
        return false;
    }


    // elokeszitjuk az utasitast
    $stmt = mysqli_prepare( $conn,"UPDATE film SET cim = ?, megjelenesiEv = ?, korhatar = ?, rendezoID = ?, studioID = ? WHERE filmID = ?");

    // bekotjuk a parametereket (igy biztonsagosabb az adatkezeles)
    mysqli_stmt_bind_param($stmt, "sddddd", $cim, $megjelenesiEv, $korhatar, $rendezoID, $studioID, $filmID);

    // lefuttatjuk az SQL utasitast
    $success = mysqli_stmt_execute($stmt);
    // ez logikai erteket ad vissza, ami megmondja, hogy sikerult-e
    // vegrehajtani az utasitast
    if($success == false){
        die(mysqli_error($conn));
    }
    mysqli_close($conn);
    return $success;
}

function modify_rendezo ($rendezoID, $rendezoNev, $szuletesiEv, $szuletesiOrszag){
    if ( !($conn = imdb_connect()) ) { // ha nem sikerult csatlakozni, akkor kilepunk
        return false;
    }


    // elokeszitjuk az utasitast
    $stmt = mysqli_prepare( $conn,"UPDATE rendezo SET rendezoNev = ?, szuletesiEv = ?, szuletesiOrszag = ? WHERE rendezoID = ?");

    // bekotjuk a parametereket (igy biztonsagosabb az adatkezeles)
    mysqli_stmt_bind_param($stmt, "sdsd", $rendezoNev, $szuletesiEv, $szuletesiOrszag, $rendezoID);

    // lefuttatjuk az SQL utasitast
    $success = mysqli_stmt_execute($stmt);
    // ez logikai erteket ad vissza, ami megmondja, hogy sikerult-e
    // vegrehajtani az utasitast
    if($success == false){
        die(mysqli_error($conn));
    }
    mysqli_close($conn);
    return $success;
}

function modify_szinesz($szineszID, $szineszNev, $szuletesiEv, $szuletesiOrszag){
    if ( !($conn = imdb_connect()) ) { // ha nem sikerult csatlakozni, akkor kilepunk
        return false;
    }


    // elokeszitjuk az utasitast
    $stmt = mysqli_prepare( $conn,"UPDATE szinesz SET szineszNev = ?, szuletesiEv = ?, szuletesiOrszag = ? WHERE szineszID = ?");

    // bekotjuk a parametereket (igy biztonsagosabb az adatkezeles)
    mysqli_stmt_bind_param($stmt, "sdsd", $szineszNev, $szuletesiEv, $szuletesiOrszag, $szineszID);

    // lefuttatjuk az SQL utasitast
    $success = mysqli_stmt_execute($stmt);
    // ez logikai erteket ad vissza, ami megmondja, hogy sikerult-e
    // vegrehajtani az utasitast
    if($success == false){
        die(mysqli_error($conn));
    }
    mysqli_close($conn);
    return $success;
}

function modify_studio($studioID, $studioNev, $alapitasiEv){
    if ( !($conn = imdb_connect()) ) { // ha nem sikerult csatlakozni, akkor kilepunk
        return false;
    }


    // elokeszitjuk az utasitast
    $stmt = mysqli_prepare( $conn,"UPDATE studio SET studioNev = ?, alapitasiEv = ? WHERE studioID = ?");

    // bekotjuk a parametereket (igy biztonsagosabb az adatkezeles)
    mysqli_stmt_bind_param($stmt, "sdd", $studioNev, $alapitasiEv, $studioID);

    // lefuttatjuk az SQL utasitast
    $success = mysqli_stmt_execute($stmt);
    // ez logikai erteket ad vissza, ami megmondja, hogy sikerult-e
    // vegrehajtani az utasitast
    if($success == false){
        die(mysqli_error($conn));
    }
    mysqli_close($conn);
    return $success;
}

function modify_szerep($filmID, $szineszID, $szerep){
    if ( !($conn = imdb_connect()) ) { // ha nem sikerult csatlakozni, akkor kilepunk
        return false;
    }


    // elokeszitjuk az utasitast
    $stmt = mysqli_prepare( $conn,"UPDATE szerep SET szerep = ? WHERE filmID = ? AND szineszID = ?");
    // bekotjuk a parametereket (igy biztonsagosabb az adatkezeles)
    mysqli_stmt_bind_param($stmt, "sdd", $szerep, $filmID, $szineszID);

    // lefuttatjuk az SQL utasitast
    $success = mysqli_stmt_execute($stmt);
    // ez logikai erteket ad vissza, ami megmondja, hogy sikerult-e
    // vegrehajtani az utasitast
    if($success == false){
        die(mysqli_error($conn));
    }
    mysqli_close($conn);
    return $success;
}

function modify_mufaj($filmID, $mufaj, $newMufaj){
    if ( !($conn = imdb_connect()) ) { // ha nem sikerult csatlakozni, akkor kilepunk
        return false;
    }

    // elokeszitjuk az utasitast
    $stmt = mysqli_prepare( $conn,"UPDATE mufaj SET mufaj = ? WHERE filmID = ? AND mufaj = ? ");
    // bekotjuk a parametereket (igy biztonsagosabb az adatkezeles)
    mysqli_stmt_bind_param($stmt, "sds", $newMufaj, $filmID, $mufaj);

    // lefuttatjuk az SQL utasitast
    $success = mysqli_stmt_execute($stmt);
    // ez logikai erteket ad vissza, ami megmondja, hogy sikerult-e
    // vegrehajtani az utasitast
    if($success == false){
        die(mysqli_error($conn));
    }
    mysqli_close($conn);
    return $success;
}

function remove_film($filmID) {


    if ( !($conn = imdb_connect()) ) { // ha nem sikerult csatlakozni, akkor kilepunk
        return false;
    }


    // elokeszitjuk az utasitast
    $stmt = mysqli_prepare( $conn,"DELETE FROM film WHERE filmID = ? ");

    // bekotjuk a parametereket (igy biztonsagosabb az adatkezeles)
    mysqli_stmt_bind_param($stmt, "d", $filmID);

    // lefuttatjuk az SQL utasitast
    $success = mysqli_stmt_execute($stmt);
    // ez logikai erteket ad vissza, ami megmondja, hogy sikerult-e
    // vegrehajtani az utasitast
    if ($success == false){
        die(mysqli_error($conn));
    }

    mysqli_close($conn);
    return $success;

}

function remove_rendezo($rendezoID) {


    if ( !($conn = imdb_connect()) ) { // ha nem sikerult csatlakozni, akkor kilepunk
        return false;
    }


    // elokeszitjuk az utasitast
    $stmt = mysqli_prepare( $conn,"DELETE FROM rendezo WHERE rendezoID = ? ");

    // bekotjuk a parametereket (igy biztonsagosabb az adatkezeles)
    mysqli_stmt_bind_param($stmt, "d", $rendezoID);

    // lefuttatjuk az SQL utasitast
    $success = mysqli_stmt_execute($stmt);
    // ez logikai erteket ad vissza, ami megmondja, hogy sikerult-e
    // vegrehajtani az utasitast
    if ($success == false){
        die(mysqli_error($conn));
    }

    mysqli_close($conn);
    return $success;

}

function remove_studio($studioID) {


    if ( !($conn = imdb_connect()) ) { // ha nem sikerult csatlakozni, akkor kilepunk
        return false;
    }


    // elokeszitjuk az utasitast
    $stmt = mysqli_prepare( $conn,"DELETE FROM studio WHERE studioID = ? ");

    // bekotjuk a parametereket (igy biztonsagosabb az adatkezeles)
    mysqli_stmt_bind_param($stmt, "d", $studioID);

    // lefuttatjuk az SQL utasitast
    $success = mysqli_stmt_execute($stmt);
    // ez logikai erteket ad vissza, ami megmondja, hogy sikerult-e
    // vegrehajtani az utasitast
    if ($success == false){
        die(mysqli_error($conn));
    }

    mysqli_close($conn);
    return $success;

}

function remove_szinesz($szineszID) {


    if ( !($conn = imdb_connect()) ) { // ha nem sikerult csatlakozni, akkor kilepunk
        return false;
    }


    // elokeszitjuk az utasitast
    $stmt = mysqli_prepare( $conn,"DELETE FROM szinesz WHERE szineszID = ? ");

    // bekotjuk a parametereket (igy biztonsagosabb az adatkezeles)
    mysqli_stmt_bind_param($stmt, "d", $szineszID);

    // lefuttatjuk az SQL utasitast
    $success = mysqli_stmt_execute($stmt);
    // ez logikai erteket ad vissza, ami megmondja, hogy sikerult-e
    // vegrehajtani az utasitast
    if ($success == false){
        die(mysqli_error($conn));
    }

    mysqli_close($conn);
    return $success;

}

function remove_mufaj($filmID, $mufaj) {


    if ( !($conn = imdb_connect()) ) { // ha nem sikerult csatlakozni, akkor kilepunk
        return false;
    }


    // elokeszitjuk az utasitast

    $stmt = mysqli_prepare( $conn,"DELETE FROM mufaj WHERE filmID = ? AND mufaj = ?");

    // bekotjuk a parametereket (igy biztonsagosabb az adatkezeles)
    mysqli_stmt_bind_param($stmt, "ds", $filmID, $mufaj);

    // lefuttatjuk az SQL utasitast
    $success = mysqli_stmt_execute($stmt);
    // ez logikai erteket ad vissza, ami megmondja, hogy sikerult-e
    // vegrehajtani az utasitast
    if ($success == false){
        die(mysqli_error($conn));
    }

    mysqli_close($conn);
    return $success;

}

function remove_szerep($filmID, $szineszID) {


    if ( !($conn = imdb_connect()) ) { // ha nem sikerult csatlakozni, akkor kilepunk
        return false;
    }


    // elokeszitjuk az utasitast
    $stmt = mysqli_prepare( $conn,"DELETE FROM szerep WHERE filmID = ? AND szineszID = ?");

    // bekotjuk a parametereket (igy biztonsagosabb az adatkezeles)
    mysqli_stmt_bind_param($stmt, "dd", $filmID, $szineszID);

    // lefuttatjuk az SQL utasitast
    $success = mysqli_stmt_execute($stmt);
    // ez logikai erteket ad vissza, ami megmondja, hogy sikerult-e
    // vegrehajtani az utasitast
    if ($success == false){
        die(mysqli_error($conn));
    }

    mysqli_close($conn);
    return $success;

}

function get_film($filmID){
    if ( !($conn = imdb_connect()) ) { // ha nem sikerult csatlakozni, akkor kilepunk
        return false;
    }

    $stmt = mysqli_prepare($conn, "SELECT * FROM film WHERE filmID = ?");

    mysqli_stmt_bind_param($stmt, "d", $filmID);

    $result = mysqli_stmt_execute($stmt);

    if( $result == false){
        die(mysqli_error($conn));
    }

    mysqli_stmt_bind_result($stmt, $filmID, $cim, $megjelenesiEv, $korhatar, $rendezoID, $studioID);

    $reader = array();

    mysqli_stmt_fetch($stmt);
    $reader["filmID"] = $filmID;
    $reader["cim"] = $cim;
    $reader["megjelenesiEv"] = $megjelenesiEv;
    $reader["korhatar"] = $korhatar;
    $reader["rendezoID"] = $rendezoID;
    $reader["studioID"] = $studioID;

    mysqli_close($conn);
    return $reader;
}

function get_szinesz($szineszID){
    if ( !($conn = imdb_connect()) ) { // ha nem sikerult csatlakozni, akkor kilepunk
        return false;
    }

    $stmt = mysqli_prepare($conn, "SELECT * FROM szinesz WHERE szineszID = ?");

    mysqli_stmt_bind_param($stmt, "d", $szineszID);

    $result = mysqli_stmt_execute($stmt);

    if( $result == false){
        die(mysqli_error($conn));
    }

    mysqli_stmt_bind_result($stmt, $szineszID, $szineszNev, $szuletesiEv, $szuletesiOrszag);

    $reader = array();

    mysqli_stmt_fetch($stmt);
    $reader["szineszID"] = $szineszID;
    $reader["szineszNev"] = $szineszNev;
    $reader["szuletesiEv"] = $szuletesiEv;
    $reader["szuletesiOrszag"] = $szuletesiOrszag;

    mysqli_close($conn);
    return $reader;
}

function get_rendezo($rendezoID){
    if ( !($conn = imdb_connect()) ) { // ha nem sikerult csatlakozni, akkor kilepunk
        return false;
    }

    $stmt = mysqli_prepare($conn, "SELECT * FROM rendezo WHERE rendezoID = ?");

    mysqli_stmt_bind_param($stmt, "d", $rendezoID);

    $result = mysqli_stmt_execute($stmt);

    if( $result == false){
        die(mysqli_error($conn));
    }

    mysqli_stmt_bind_result($stmt, $rendezoID, $rendezoNev, $szuletesiEv, $szuletesiOrszag);

    $reader = array();

    mysqli_stmt_fetch($stmt);
    $reader["rendezoID"] = $rendezoID;
    $reader["rendezoNev"] = $rendezoNev;
    $reader["szuletesiEv"] = $szuletesiEv;
    $reader["szuletesiOrszag"] = $szuletesiOrszag;

    mysqli_close($conn);
    return $reader;
}

function get_studio($studioID){
    if ( !($conn = imdb_connect()) ) { // ha nem sikerult csatlakozni, akkor kilepunk
        return false;
    }

    $stmt = mysqli_prepare($conn, "SELECT * FROM studio WHERE studioID = ?");

    mysqli_stmt_bind_param($stmt, "d", $studioID);

    $result = mysqli_stmt_execute($stmt);

    if( $result == false){
        die(mysqli_error($conn));
    }

    mysqli_stmt_bind_result($stmt, $studioID, $studioNev, $alapitasiEv);

    $reader = array();

    mysqli_stmt_fetch($stmt);
    $reader["studioID"] = $studioID;
    $reader["studioNev"] = $studioNev;
    $reader["alapitasiEv"] = $alapitasiEv;

    mysqli_close($conn);
    return $reader;
}

function get_szerep($filmID, $szineszID){
    if ( !($conn = imdb_connect()) ) { // ha nem sikerult csatlakozni, akkor kilepunk
        return false;
    }

    $stmt = mysqli_prepare($conn, "SELECT szerep.filmID, szerep.szineszID, szerep.szerep FROM szerep, film, szinesz WHERE szerep.filmID = ? AND szerep.szineszID = ? AND film.filmID = szerep.filmID AND szinesz.szineszID = szerep.szineszID");

    mysqli_stmt_bind_param($stmt, "dd", $filmID, $szineszID);

    $result = mysqli_stmt_execute($stmt);

    if( $result == false){
        die(mysqli_error($conn));
    }

    mysqli_stmt_bind_result($stmt, $filmID, $szineszID, $szerep);

    $reader = array();

    mysqli_stmt_fetch($stmt);
    $reader["filmID"] = $filmID;
    $reader["szineszID"] = $szineszID;
    $reader["szerep"] = $szerep;

    mysqli_close($conn);
    return $reader;
}

function get_mufaj($filmID, $mufaj){
    if ( !($conn = imdb_connect()) ) { // ha nem sikerult csatlakozni, akkor kilepunk
        return false;
    }

    $stmt = mysqli_prepare($conn, "SELECT * FROM mufaj WHERE filmID = ? AND mufaj = ? ");

    mysqli_stmt_bind_param($stmt, "ds", $filmID, $mufaj);

    $result = mysqli_stmt_execute($stmt);

    if( $result == false){
        die(mysqli_error($conn));
    }

    mysqli_stmt_bind_result($stmt, $filmID, $mufaj);

    $reader = array();

    mysqli_stmt_fetch($stmt);
    $reader["filmID"] = $filmID;
    $reader["mufaj"] = $mufaj;

    mysqli_close($conn);
    return $reader;
}


//END