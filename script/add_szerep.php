<?php

    include('fuggvenyek.php');
    imdb_connect();

    $v_filmID = $_POST['filmID'];
    $v_szineszID = $_POST['szineszID'];
    $v_szerep = $_POST['szerep'];

    if (isset($v_filmID) && isset($v_szineszID) &&
        isset($v_szerep)) {

        $v_clear_filmID = htmlspecialchars($v_filmID);
        $v_clear_szineszID = htmlspecialchars($v_szineszID);
        $v_clear_szerep = htmlspecialchars($v_szerep);

        // beszúrjuk az új rekordot az adatbázisba
        $sikeres = add_szerep($v_clear_filmID, $v_clear_szineszID, $v_clear_szerep);

        if ($sikeres == false) {
            die("Nem sikerült felvinni rekordot.");
        } else {
            header("Location: list_szerep.php");
        }
    } else {
        error_log("Nincs beállítva valamely érték");
    }
