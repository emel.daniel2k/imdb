<?php
include('fuggvenyek.php');
imdb_connect();

$v_filmID = $_POST['filmID'];

if ( isset($v_filmID)) {
    // beszúrjuk az új rekordot az adatbázisba
    $v_clear_filmID = htmlspecialchars($v_filmID);

    $success = remove_film($v_clear_filmID);
    if($success == false){
        die("Nem sikerült törölni a rekordot.");
    } else {
        header("Location: list_film.php");
    }
} else {
    error_log("Nincs beállítva valamely érték");

}