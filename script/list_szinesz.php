<?php
    include('connect.php');
    include('header.php');



    if ( mysqli_select_db($conn, 'imdb')){

        $sql = "SELECT * FROM szinesz";
        $res = mysqli_query($conn, $sql) or die("Hibás utasítás!");

        //html
        echo '<table class="table table-light table-striped">';
        echo '<thead class="thead-dark">';
        echo '<tr>';
        echo '<th scope="col">Azonosítószám</th>';
        echo '<th scope="col">Név</th>';
        echo '<th scope="col">Születési Év</th>';
        echo '<th scope="col">Születési ország</th>';
        echo '<th scope="col">Módosítás</th>';
        echo '<th scope="col">Törlés</th>';
        echo '</tr>';
        echo '</thead>';
        echo '<tbody>';

        while(($current_row = mysqli_fetch_assoc($res))) {
            echo '<tr>';
            echo '<td>' . $current_row["szineszID"] .'</td>';
            echo '<td>' . $current_row["szineszNev"] . '</td>';
            echo '<td>' . $current_row["szuletesiEv"] . '</td>';
            echo '<td>' . $current_row["szuletesiOrszag"] . '</td>';
            echo'<form method="post" id="modify_szinesz" action="modify_szinesz_form.php" accept-charset="UTF-8">';
            echo '<td><button type="submit" form="modify_szinesz" class="btn btn-warning" name="szineszID" value="'.$current_row["szineszID"].'">Módosítás</td>';
            echo '</form>';
            echo'<form method="post" id="delete_szinesz" action="delete_szinesz.php" accept-charset="UTF-8">';
            echo '<td><button type="submit" form="delete_szinesz" class="btn btn-danger" name="szineszID" value="'.$current_row["szineszID"].'">Törlés</td>';
            echo '</form>';
            echo '</tr>';
        }
        echo '</tbody>';
        echo '</table>';

        mysqli_free_result($res);
    } else {
        die('Nem sikerlt csatlakozni az adatbázishoz');
    }

    mysqli_close($conn);

    include('footer.php');
// END