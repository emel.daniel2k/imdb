<?php

include('fuggvenyek.php');
imdb_connect();

$v_filmID = $_POST['filmID'];
$v_mufaj = $_POST['mufaj'];
$v_newMufaj = $_POST['newMufaj'];

if (isset($v_filmID) && isset($v_mufaj) && isset($v_newMufaj)) {

    $v_clear_filmID = htmlspecialchars($v_filmID);
    $v_clear_mufaj = htmlspecialchars($v_mufaj);
    $v_clear_newMufaj = htmlspecialchars($v_newMufaj);

    // beszúrjuk az új rekordot az adatbázisba
    $sikeres = modify_mufaj($v_clear_filmID, $v_clear_mufaj, $v_clear_newMufaj);

    if ($sikeres == false) {
        die("Nem sikerült szerkeszteni a rekordot.");
    } else {
        header("Location: list_mufaj.php");
    }
} else {
    error_log("Nincs beállítva valamely érték");
}
//END