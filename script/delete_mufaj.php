<?php
include('fuggvenyek.php');
imdb_connect();
$v_filmID = $_POST['filmID'];
$v_mufaj = $_POST['mufaj'];

if ( isset($v_mufaj) && isset($v_filmID)) {
    // beszúrjuk az új rekordot az adatbázisba
    $v_clear_filmID = htmlspecialchars($v_filmID);
    $v_clear_mufaj = htmlspecialchars($v_mufaj);

    $success = remove_mufaj($v_clear_filmID, $v_clear_mufaj);
    if($success == false){
        die("Nem sikerült törölni a rekordot.");
    } else {
        header("Location: list_mufaj.php");
    }
} else {
    error_log("Nincs beállítva valamely érték");
}
