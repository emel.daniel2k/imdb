<?php
    include('fuggvenyek.php');
    imdb_connect();

    $v_filmID = $_POST['filmID'];
    $v_mufaj = $_POST['mufaj'];

    if (isset($v_filmID) && isset($v_mufaj)) {

        $v_clear_filmID = htmlspecialchars($v_filmID);
        $v_clear_mufaj = htmlspecialchars($v_mufaj);

        // beszúrjuk az új rekordot az adatbázisba
        $sikeres = add_mufaj($v_clear_filmID, $v_clear_mufaj);

        if ($sikeres == false) {
            die("Nem sikerült felvinni rekordot.");
        } else {
            header("Location: list_mufaj.php");
        }
    } else {
        error_log("Nincs beállítva valamely érték");
    }
//END