<?php
include("connect.php");
include("fuggvenyek.php");
?>
<?php
$v_rendezoID = $_POST["rendezoID"];
$v_clear_rendezoID = htmlspecialchars($v_rendezoID);
$rendezoadat = get_rendezo($v_clear_rendezoID);

?>
    <h3>Rendező Szerkesztés:</h3>
    <form method="post" class="form-inline" id="rendezoForm" action="modify_rendezo_script.php" accept-charset="UTF-8">
        <label for="rendezoID">Rendező ID</label>
        <?php echo '<input type="number" id="rendezoID" name="rendezoID" min="1" max="9999" value="'.$rendezoadat["rendezoID"].'" readonly="readonly"/>';?>
        <label for="rendezoNev">Név</label>
        <?php echo '<input type="text" id="rendezoNev" name="rendezoNev" value="'.$rendezoadat["rendezoNev"].'"/>';?>
        <label for="szuletesiEv">Születési Év</label>
        <?php echo '<input type="number" id="szuletesiEv" name="szuletesiEv" min="1900" max="2020" value="'.$rendezoadat["szuletesiEv"].'"/>';?>
        <label for="szuletesiOrszag">Születési Ország</label>
        <?php echo '<input type="text" id="szuletesiOrszag" name="szuletesiOrszag" value="'.$rendezoadat["szuletesiOrszag"].'"/>';?>
        <button type="submit" form="rendezoForm" class="btn btn-primary">Szerkesztés</button>
    </form>
<?php mysqli_close($conn); ?>