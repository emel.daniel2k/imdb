<?php

include('fuggvenyek.php');
imdb_connect();

$v_filmID = $_POST['filmID'];
$v_cim = $_POST['cim'];
$v_megjelenesiEv = $_POST['megjelenesiEv'];
$v_korhatar = $_POST['korhatar'];
$v_rendezoID = $_POST['rendezoID'];
$v_studioID = $_POST['studioID'];

if (isset($v_filmID) && isset($v_cim) &&
    isset($v_megjelenesiEv) && isset($v_korhatar) && isset($v_rendezoID) && isset($v_studioID)){

    $v_clear_filmID = htmlspecialchars($v_filmID);
    $v_clear_cim = htmlspecialchars($v_cim);
    $v_clear_megjelenesiEv = htmlspecialchars($v_megjelenesiEv);
    $v_clear_korhatar = htmlspecialchars($v_korhatar);
    $v_clear_rendezoID = htmlspecialchars($v_rendezoID);
    $v_clear_studioID = htmlspecialchars($v_studioID);

    // beszúrjuk az új rekordot az adatbázisba
    $sikeres = modify_film($v_clear_filmID, $v_clear_cim, $v_clear_megjelenesiEv, $v_clear_korhatar, $v_clear_rendezoID, $v_clear_studioID);

    if ($sikeres == false) {
        die("Nem sikerült módosítani rekordot.");
    } else {
        header("Location: list_film.php");
    }
} else {
    error_log("Nincs beállítva valamely érték");
}