<?php

include('fuggvenyek.php');
imdb_connect();

$v_rendezoID = $_POST['rendezoID'];

if (isset($v_rendezoID)) {
    // beszúrjuk az új rekordot az adatbázisba
    $v_clear_rendezoID = htmlspecialchars($v_rendezoID);

    $success = remove_rendezo($v_clear_rendezoID);
    if ($success == false) {
        die("Nem sikerült törölni a rekordot.");
    } else {
        header("Location: list_rendezo.php");
    }
} else {
    error_log("Nincs beállítva valamely érték");

}