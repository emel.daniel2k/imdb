<?php
echo '<!doctype html>
<html lang="hu">
    <head>
        <!-- Required meta tags -->
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <title>Adatbázis</title>
        <meta name="description" content="imdb">
        <meta name="author" content="Emel Dániel">
        <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/css/bootstrap.min.css" integrity="sha384-TX8t27EcRE3e/ihU7zmQxVncDAy5uIKz4rEkgIXeMed4M0jlfIDPvg6uqKI2xXr2" crossorigin="anonymous">
        <link href="../styles/adatbazis.css" rel="stylesheet"/>
    </head>
    <body>

        <nav class="navbar navbar-expand-lg navbar-light fixed-top" id="navbar">
            <a class="navbar-brand" href="../index.html"><img src="../images/logo_adatb.png" width="120" height="43" alt="mozi"></a>
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>
            <div class="collapse navbar-collapse" id="navbarNav">
                <ul class="nav navbar-nav navbar-right">
                    <li class="nav-item">
                        <a class="nav-link" href="../index.html">Főoldal<span class="sr-only">(current)</span></a>
                    </li>
                    <li class="nav-item active">
                        <a class="nav-link" href="../adatbazis.html">Adatbázis</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" id="back" href="../adatbazis.html">Vissza</a>
                    </li>
                </ul>
            </div>
        </nav>';
// END