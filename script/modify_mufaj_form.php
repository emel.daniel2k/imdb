<?php
include("connect.php");
include("fuggvenyek.php");
?>
<?php
$v_filmID = $_POST["filmID"];
$v_mufaj = $_POST["mufaj"];
$v_clear_filmID = htmlspecialchars($v_filmID);
$v_clear_mufaj = htmlspecialchars($v_mufaj);
$mufajadat = get_mufaj($v_clear_filmID, $v_clear_mufaj); ?>

<h3>Műfaj szerkesztése:</h3>
<form method="post" id="mufajForm" class="form-inline" action="modify_mufaj_script.php" accept-charset="UTF-8">
    <label for="filmID">Film ID:</label>
    <?php echo '<input type="number" id="filmID" name="filmID" min="1" max="9999" value="'.$mufajadat["filmID"].'" readonly="readonly"/>';?>
    <label for="newMufaj">Műfaj:</label>
    <?php echo '<input type="hidden" name="mufaj" value="'.$mufajadat['mufaj'].'"/>';?>
    <?php echo'<input type="text" id="mufaj" name="newMufaj" value="'.$mufajadat["mufaj"].'"/>';?>
    <button type="submit" form="mufajForm" class="btn btn-primary">Szerkesztés</button>
</form>
