<?php

    include('fuggvenyek.php');
    imdb_connect();

    $v_studioID = $_POST['studioID'];
    $v_studioNev = $_POST['studioNev'];
    $v_alapitasiEv = $_POST['alapitasiEv'];

    if (isset($v_studioID) && isset($v_studioNev) &&
        isset($v_alapitasiEv)) {

        $v_clear_studioID = htmlspecialchars($v_studioID);
        $v_clear_studioNev = htmlspecialchars($v_studioNev);
        $v_clear_alapitasiEv = htmlspecialchars($v_alapitasiEv);

        // beszúrjuk az új rekordot az adatbázisba
        $sikeres = add_studio($v_clear_studioID, $v_clear_studioNev, $v_clear_alapitasiEv);

        if ($sikeres == false) {
            die("Nem sikerült felvinni rekordot.");
        } else {
            header("Location: list_studio.php");
        }
    } else {
        error_log("Nincs beállítva valamely érték");
    }
