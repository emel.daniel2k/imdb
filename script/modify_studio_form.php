<?php
include("connect.php");
include("fuggvenyek.php");
?>
<?php
$v_studioID = $_POST["studioID"];
$v_clear_studioID = htmlspecialchars($v_studioID);
$studioadat = get_studio($v_clear_studioID); ?>


<h3>Stúdió szerkesztése:</h3>
<form method="post" id="studioForm" class="form-inline" action="modify_studio_script.php" accept-charset="UTF-8">
    <label for="studioID1">Stúdió ID:</label>
    <?php echo '<input type="number" id="studioID1" name="studioID" min="1" max="9999" value="'.$studioadat["studioID"].'" readonly="readonly"/>';?>
    <label for="studioNev">Név:</label>
    <?php echo '<input type="text" id="studioNev" name="studioNev" value="'.$studioadat["studioNev"].'"/>';?>
    <label for="alapitasiEv">Alapítási Év:</label>
    <?php echo '<input type="number" id="alapitasiEv" name="alapitasiEv" min="1900" max="2020" value="'.$studioadat["alapitasiEv"].'"/>';?>
    <button type="submit" form="studioForm" class="btn btn-primary">Szerkesztés</button>
</form>