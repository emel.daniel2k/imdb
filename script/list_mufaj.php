<?php

include('header.php');
include('connect.php');

if ( mysqli_select_db($conn, 'imdb')){

    $sql = "SELECT * FROM mufaj";
    $res = mysqli_query($conn, $sql) or die("Hibás utasítás!");

    //html
    echo '<table class="table table-light table-striped">';
    echo '<thead class="thead-dark">';
    echo '<tr>';
    echo '<th scope="col">Film azonosítószám</th>';
    echo '<th scope="col">Műfaj</th>';
    echo '<th scope="col">Módosítás</th>';
    echo '<th scope="col">Törlés</th>';
    echo '</tr>';
    echo '</thead>';
    echo '<tbody>';

    while(($current_row = mysqli_fetch_assoc($res))) {
        echo '<tr>';
        echo '<td>' . $current_row["filmID"] .'</td>';
        echo '<td>' . $current_row["mufaj"] . '</td>';
        echo'<form method="post"  action="modify_mufaj_form.php" accept-charset="UTF-8">';
        echo '<input type="hidden" name="filmID" value="'.$current_row["filmID"].'">';
        echo '<td><button type="submit"  class="btn btn-warning" name="mufaj" value="'.$current_row["mufaj"].'">Módosítás</td>';
        echo '</form>';
        echo'<form method="post"  action="delete_mufaj.php" accept-charset="UTF-8">';
        echo '<input type="hidden" name="filmID" value="'.$current_row["filmID"].'">';
        echo '<td><button type="submit"  class="btn btn-danger" name="mufaj" value="'.$current_row["mufaj"].'">Törlés</td>';
        echo '</form>';
        echo '</tr>';
    }
    echo '</tbody>';
    echo '</table>';

    mysqli_free_result($res);
} else {
    die('Nem sikerlt csatlakozni az adatbázishoz');
}

mysqli_close($conn);

include('footer.php');
//END
