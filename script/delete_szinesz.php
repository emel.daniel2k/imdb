<?php

include('fuggvenyek.php');
imdb_connect();

$v_szineszID = $_POST['szineszID'];

if (isset($v_szineszID)) {
    // beszúrjuk az új rekordot az adatbázisba
    $v_clear_szineszID = htmlspecialchars($v_szineszID);

    $success = remove_szinesz($v_clear_szineszID);
    if ($success == false) {
        die("Nem sikerült törölni a rekordot.");
    } else {
        header("Location: list_szinesz.php");
    }
} else {
    error_log("Nincs beállítva valamely érték");

}