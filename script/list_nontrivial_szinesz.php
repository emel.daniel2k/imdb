<?php
include('header.php');
include('connect.php');

if ( mysqli_select_db($conn, 'imdb')){
    $sql = "SELECT szinesz.szineszID, szinesz.szineszNev, COUNT(szerep.szerep) AS 'szerepSzam' FROM szinesz, szerep WHERE szinesz.szineszID = szerep.szineszID GROUP BY szinesz.szineszID";
    $res = mysqli_query($conn, $sql) or die("Hibás utasítás!");

    //html
    echo '<table class="table table-light table-striped">';
    echo '<thead class="thead-dark">';
    echo '<tr>';
    echo '<th scope="col">Színész Azonosító</th>';
    echo '<th scope="col">Színész Név</th>';
    echo '<th scope="col">Szerepek Száma</th>';
    echo '<th scope="col">Módosítás</th>';
    echo '<th scope="col">Törlés</th>';
    echo '</tr>';
    echo '</thead>';
    echo '<tbody>';

    while(($current_row = mysqli_fetch_assoc($res))) {
        echo '<tr>';
        echo '<td>' . $current_row["szineszID"] .'</td>';
        echo '<td>' . $current_row["szineszNev"] . '</td>';
        echo '<td>' . $current_row["szerepSzam"] . '</td>';
        echo '<form method="post" id="modify_film" action="modify_szinesz_form.php" accept-charset="UTF-8">';
        echo '<td><button type="submit" form="modify_film" class="btn btn-warning" name="szineszID" value="'.$current_row["szineszID"].'">Módosítás</td>';
        echo '</form>';
        echo'<form method="post" id="delete_film" action="delete_szinesz.php" accept-charset="UTF-8">';
        echo '<td><button type="submit" form="delete_film" class="btn btn-danger" name="szineszID" value="'.$current_row["szineszID"].'">Törlés</td>';
        echo '</form>';
        echo '</tr>';
    }
    echo '</tbody>';
    echo '</table>';

    mysqli_free_result($res);
} else {
    die('Nem sikerlt csatlakozni az adatbázishoz');
}

mysqli_close($conn);

include('footer.php');