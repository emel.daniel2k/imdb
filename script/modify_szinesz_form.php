<?php
include("connect.php");
include("fuggvenyek.php");
?>
<?php
$v_szineszID = $_POST["szineszID"];
$v_clear_szineszID = htmlspecialchars($v_szineszID);
$szineszadat = get_szinesz($v_clear_szineszID);

?>
    <h3>Színész szerkesztése:</h3>
    <form method="post" class="form-inline" id="szineszForm" action="modify_szinesz_script.php" accept-charset="UTF-8">
        <label for="szineszID">Színész ID</label>
        <?php echo '<input type="number" id="szineszID" name="szineszID" min="1" max="9999" value="'.$szineszadat["szineszID"].'" readonly="readonly"/>';?>
        <label for="szineszNev">Név</label>
        <?php echo '<input type="text" id="szineszNev" name="szineszNev" value="'.$szineszadat["szineszNev"].'"/>';?>
        <label for="szuletesiEv">Születési Év</label>
        <?php echo '<input type="number" id="szuletesiEv" name="szuletesiEv" min="1900" max="2020" value="'.$szineszadat["szuletesiEv"].'"/>';?>
        <label for="szuletesiOrszag">Születési Ország</label>
        <?php echo '<input type="text" id="szuletesiOrszag" name="szuletesiOrszag" value="'.$szineszadat["szuletesiOrszag"].'"/>';?>
        <button type="submit" form="szineszForm" class="btn btn-primary">Szerkesztés</button>
    </form>
<?php mysqli_close($conn); ?>